/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util.users;

import ec.edu.espol.util.Util;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Jose
 */
public abstract class Persona {
    protected String nombres;
    protected String apellidos;
    protected String email;
    protected String organizacion;
    protected String usuario;
    protected String contraseña;
    // CONSTRUCTOR
    public Persona(){}
    public Persona(String nombre, String apellido, String email, String organizacion, String usuario, String contraseña){
        this.nombres = nombre;
        this.apellidos = apellido;
        this.email = email;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.contraseña = contraseña;
    }
    // GETTERS Y SETTERS
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    // MÉTODOS
        public static void comprobarRegistro(String a, String[] b, String c, ArrayList<String> cads){
        if (Arrays.asList(b).indexOf(c) == 2 || Arrays.asList(b).indexOf(c) == 4){
            while (true){
                String cad = Util.pedirCad();
                if (Util.comprobarNoEnFile(a,cad)){
                    cads.add(cad);
                    break;}
                else
                    System.out.println("Ese nombre de usuario o correo electrónico ya fue registrado, intentelo de nuevo.");
            }
        }
        else{
        String cad = Util.pedirCad();
        cads.add(cad);
        }
    }
    public static void comprobarUsuarioyClave(String a) throws NoSuchAlgorithmException{
        while(true){
            System.out.println("Ingrese su usuario: ");
            String usuario = Util.pedirCad();
            System.out.println("Ingrese su contraseña:");
            String contra = Util.pedirCad();
            String[] terminos;
            if(Util.comprobarNoEnFile(a, usuario))
                System.out.println("Usuario o contraseña incorrectos.\nAsegurese que tiene una cuenta de vendedor creada.");
            else if(!Util.comprobarNoEnFile(a, usuario)){
                terminos = Util.lineaConPalabra(a, usuario);
                if(terminos[1].equals(usuario) && terminos[2].equals(Util.calcularHash(contra)))
                    break;
                else
                 System.out.println("Usuario o contraseña incorrectos.");
            }
        }
    }
        public static String comprobarUsuarioyClaveParaCorreo(String a) throws NoSuchAlgorithmException{
        String[] terminos;
        while(true){
            System.out.println("Ingrese su usuario: ");
            String usuario = Util.pedirCad();
            System.out.println("Ingrese su contraseña:");
            String contra = Util.pedirCad();
            if(Util.comprobarNoEnFile(a, usuario))
                System.out.println("Usuario o contraseña incorrectos.\nAsegurese que tiene una cuenta de vendedor creada.");
            else if(!Util.comprobarNoEnFile(a, usuario)){
                terminos = Util.lineaConPalabra(a, usuario);
                if(terminos[1].equals(usuario) && terminos[2].equals(Util.calcularHash(contra)))
                    break;
                else
                 System.out.println("Usuario o contraseña incorrectos.");
            }
        }
        return terminos[0];
    }
    
      public static void registrar(String archivo, ArrayList<String> p){
        try(FileWriter fw = new FileWriter(archivo,true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw)){
            out.println(p.get(2)+";"+p.get(4)+";"+Util.calcularHash(p.get(5)));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    // SOBREESCRITURAS
    @Override
    public String toString(){
        String cad = "Nombres: " + nombres + "\nApellidos: " + apellidos + "\nCorreo electrónico: " + email +
                "\nOrganización: " + organizacion + "\nUsuario: " + usuario + "\nContraseña: " + contraseña;
        return cad;
    }
    
    @Override
    public boolean equals(Object o){
        if (o == null)
            return false;
        if (this == o)
            return true;
        if (this.getClass() != o.getClass())
            return false;
        Persona other = (Persona)o;
        if (this.usuario.equals(other.usuario))
            return true;
        return false;
    }
}
