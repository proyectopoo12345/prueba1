/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util.items;

import ec.edu.espol.util.Util;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Jose
 */
public abstract class Vehiculo {
    protected String placa;
    protected int año;
    protected double recorrido;
    protected String color;
    protected String transmision;
    protected double precio;
    
    public Vehiculo(String placa,int año, double recorrido, String color, String transmision,double precio){
        this.placa = placa;
        this.año = año;
        this.recorrido = recorrido;
        this.color = color;
        this.transmision = transmision;
        this.precio = precio;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public double getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(double recorrido) {
        this.recorrido = recorrido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
    
    public static void comprobarRegistro(String a, String[] b, String c, ArrayList datos){
        if (Arrays.asList(b).indexOf(c) == 0){
            while (true){
                String cad = Util.pedirCad();
                if (Util.comprobarNoEnFile(a,cad)){
                    datos.add(cad);
                    break;}
                else
                    System.out.println("Ese número de matrícula ya esta registrado, intentelo de nuevo.");
            }
        }
        else if (Arrays.asList(b).indexOf(c) == 4){
                int num = Util.pedirIntPositivo();
                datos.add(num);
        }
        else if (Arrays.asList(b).indexOf(c) == 5 || Arrays.asList(b).indexOf(c) == Arrays.asList(b).size()-1){
                double num = Util.pedirDoublePositivo();
                datos.add(num);}
        else{
        String cad = Util.pedirCad();
        datos.add(cad);
        }
    }
    public static void registrar(String archivo, ArrayList p){
        try(FileWriter fw = new FileWriter(archivo,true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw)){
            p.forEach((element) -> {
                out.print(element+";");
            });
                out.println("");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    public static double MaxPrecio(ArrayList<Vehiculo> array){
        ArrayList<Double> precios = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            precios.add(vehiculo.precio);
        }
        double max = precios.get(0);
        for(double precio:precios){
            if (max<precio)
                max = precio;
        }
        return max;
    }
    public static double MinPrecio(ArrayList<Vehiculo> array){
        ArrayList<Double> precios = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            precios.add(vehiculo.precio);
        }
        double min = precios.get(0);
        for(double preci:precios){
            if (min>preci)
                min = preci;
        }
        return min;
    }
    public static double MaxRecorrido(ArrayList<Vehiculo> array){
        ArrayList<Double> recorridos = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            recorridos.add(vehiculo.recorrido);
        }
        double max = recorridos.get(0);
        for(double reco:recorridos){
            if (max<reco)
                max = reco;
        }
        return max;
    }
    public static double MinRecorrido(ArrayList<Vehiculo> array){
        ArrayList<Double> recorridos = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            recorridos.add(vehiculo.recorrido);
        }
        double min = recorridos.get(0);
        for(double reco:recorridos){
            if (min>reco)
                min = reco;
        }
        return min;
    }
    public static int MaxAño(ArrayList<Vehiculo> array){
        ArrayList<Integer> años = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            años.add(vehiculo.año);
        }
        int max = años.get(0);
        for(int añ:años){
            if (max<añ)
                max = añ;
        }
        return max;
    }
    public static int MinAño(ArrayList<Vehiculo> array){
        ArrayList<Integer> años = new ArrayList<>();
        for (Vehiculo vehiculo:array){
            años.add(vehiculo.año);
        }
        int min = años.get(0);
        for(int añ:años){
            if (min>añ)
                min = añ;
        }
        return min;
    }
    
    public static int seleccionarTipo(){
        int num = Util.pedirIntEnIntervalo(1, 4);
        return num;
    }
    
    @Override
    public abstract String toString();
    
    @Override
    public boolean equals(Object o){
        if (o == null)
            return false;
        if (this == o)
            return true;
        if (this.getClass() != o.getClass())
            return false;
        Vehiculo other = (Vehiculo)o;
        if (this.placa.equals(other.placa))
            return true;
        return false;
    }
}
