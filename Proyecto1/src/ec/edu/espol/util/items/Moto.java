/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util.items;

/**
 *
 * @author Jose
 */
public class Moto extends Vehiculo{
    private String marca;
    private String modelo;
    private String motor;
    private String combustible;
    
    public Moto(String placa,String marca,String modelo,String motor,int año,double recorrido,String color,String combustible,String transmision,double precio){
        super(placa,año,recorrido,color,transmision,precio);
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.combustible = combustible;
    }
    @Override
    public String toString(){
        String cad = "Moto\nMatrícula del vehículo: " + this.placa + "\nMarca: " + this.marca +
                "\nModelo del vehículo: "+this.modelo+"\nTipo de motor: " + this.motor + "\nAño del vehículo: " + this.año +
                "\nRecorrido en km: " + this.recorrido + "\nTipo de combustible: " + this.combustible +
                "\nTransmisión del vehículo: " + this.transmision + "\nPrecio: " + this.precio +"$";
        return cad;
    }
}
