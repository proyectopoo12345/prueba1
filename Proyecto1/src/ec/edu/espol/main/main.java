/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.main;
import ec.edu.espol.util.Oferta;
import ec.edu.espol.util.Util;
import ec.edu.espol.util.items.Auto;
import ec.edu.espol.util.items.Camion;
import ec.edu.espol.util.items.Camioneta;
import ec.edu.espol.util.items.Moto;
import ec.edu.espol.util.items.Vehiculo;
import ec.edu.espol.util.users.Comprador;
import ec.edu.espol.util.users.Persona;
import ec.edu.espol.util.users.Vendedor;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Jose
 */
public class main {

    /**
     * @param args the command line arguments
     * @throws java.security.NoSuchAlgorithmException
     */
    public static void main(String[] args) throws NoSuchAlgorithmException {
        File archivo1 = new File("vendedores");
        File archivo2 = new File("compradores");
        File archivo3 = new File("vehiculos");
        File archivo4 = new File("ofertas");
        //archivo1.delete();
        //archivo2.delete();
        //archivo3.delete();
        //archivo4.delete()
        try { 
            archivo1.createNewFile();
            archivo2.createNewFile();
            archivo3.createNewFile();
            archivo4.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Debe haber una forma más eficiente de crear archivos de texto vacios cada vez que se corre la app
        
        
        /*
        int[] nums = Util.pedirIntPosIntervalo(1, 10); //Son pruebas
        System.out.println(nums[0]);
        System.out.println(nums[1]);
        Vendedor a1 = new Vendedor("Ricardo Oscar","Fernandez Pérez","rfernandez@espol.edu.ec","ESPOL","ElrichMC","123456a");
        Vendedor a2 = new Vendedor("José","Díaz","jdiaz@espol.edu.ec","ESPOL","pidudiaz","111111");
        Vendedor a3 = new Vendedor("Guido","Becerra","gbecerra@espol.edu.ec","ESPOL","prepucio","cocoman");
        System.out.println(a1);
        a1.registrar("vendedores");
        a2.registrar("vendedores");
        a3.registrar("vendedores");
        System.out.println(Util.comprobarNoEnFile("vendedores", "pidudiaz"));
        */
        
        System.out.println("Bienvenido a AutoTech, ¡Su mejor app de compra y venta de vehículos usados!");
        String[] caddatos ={"sus nombres:","sus apellidos:","su correo electrónico:","su organización:","su nombre de usuario:","su contraseña: "};
        String[] caddatosmoto ={"la matrícula ","la marca ","el modelo ","el motor ","el año ","el recorrido ","el color ",
                 "el tipo de combustible ","la transmisión ","el precio inicial "};
        String[] caddatos4ruedas ={"la matrícula ","la marca ","el modelo ","el motor ","el año ","el recorrido ","el color ",
                 "el tipo de combustible ","el tipo de vidrio ","la transmisión ","el precio inicial "};
        ArrayList<Vendedor> vendedores = new ArrayList<>(); //Aun no sé si deberiamos hacer un array con los objetos vendedor, o es suficiente con tenerlos registrados en el txt,
                                                            // O si deberiamos poner en el txt todos los datos del vendedor, tal que cada linea cuente como un vendedor (o sea que en vez de tener solo
                                                            // el correo electronico, nombre de usuario y contraseña en el txt, poner los 6 atributos y leerlos)
        ArrayList<Comprador> compradores = new ArrayList<>();
        ArrayList<Vehiculo> vehiculos = new ArrayList<>();
        
        //ESTOS SON EJEMPLOS PARA UTILIZAR LA FUNCION DE OFERTAR DEL COMPRADOR, NO SE ESCRIBEN EN EL DOCUMENTO DE VEHICULOS

        Moto mo = new Moto("0001","toyota","A1","2 nucleos",2001,10000.0,"caca","caca","caca",9000.0);
        vehiculos.add(mo);
        Camion ca = new Camion("0002","Motorola","B2","6 nucleos",2010,35000.0,"caca","caca","caca","caca",20000.0);
        vehiculos.add(ca);
        Auto au = new Auto("0003","Samsung","Escarabajo","4 nucleos",2015,75000.0,"caca","caca","caca","caca",16000.0);
        vehiculos.add(au);

        
        
        
        
       
        boolean salir1 = false;
        while(!salir1){  //Estos while se necesitan para que se repita el switch cuando se hace para atras o se quiere tomar otra opcion
        boolean salir2 = false; //Es importante que esto se replantee por si el usuario entra en comprador o vendedor y despues regresa a la página original
        boolean salir3 = false;
        System.out.println("Seleccione una opción del menú: ");
        System.out.println("1. Vendedor"+"\n2. Comprador"+"\n3. Salir");
        int select = Util.pedirIntEnIntervalo(1,3);
        switch(select){
            case 1:
                while(!salir2){
                System.out.println("Seleccione una opción del menú: ");
                System.out.println("1. Registrar un nuevo vendedor"+"\n2. Ingresar un nuevo vehículo"+"\n3. Aceptar oferta"+"\n4. Regresar");
                int select2 = Util.pedirIntEnIntervalo(1,4);
                switch(select2){ //OPCIONES DEL VENDEDOR
                    case 1: // REGISTRAR UN NUEVO VENDEDOR (COMPLETO)
                        ArrayList<String> cadsV = new ArrayList<>(); //aqui se almacenan temporalmente los datos de entrada para despues armar un nuevo vendedor
                        for(String ele:caddatos){
                            System.out.println("Ingrese "+ele);
                            Persona.comprobarRegistro("vendedores" ,caddatos, ele, cadsV);
                        }
                        Vendedor nuevo = new Vendedor(cadsV.get(0),cadsV.get(1),cadsV.get(2),cadsV.get(3),cadsV.get(4),cadsV.get(5));
                        Persona.registrar("vendedores", cadsV);
                        vendedores.add(nuevo);
                        System.out.println("\n" + nuevo + "\n");
                        break;
                    case 2: // INGRESAR UN NUEVO VEHÍCULO (COMPLETO) (SE PUEDE OPTIMIZAR)
                        Persona.comprobarUsuarioyClave("vendedores");
                        System.out.println("Ingrese que tipo de vehiculo desea ingresar.\n1. Auto.\n2. Camion.\n3. Camioneta.\n4. Moto.\n5. Regresar.");
                        int opcion = Util.pedirIntEnIntervalo(1, 5);
                        boolean opcion1 = true;
                        while(opcion1){
                        switch(opcion){
                            case 1:
                                ArrayList a = new ArrayList();
                                for (String ele: caddatos4ruedas){
                                    System.out.println("Ingrese "+ele+"del vehículo: ");
                                    Vehiculo.comprobarRegistro("vehiculos", caddatos4ruedas, ele, a);
                                }
                                Auto auto = new Auto((String)a.get(0),(String)a.get(1),(String)a.get(2),(String)a.get(3),(int)a.get(4),
                                (double)a.get(5),(String)a.get(6),(String)a.get(7),(String)a.get(8),(String)a.get(9),(double)a.get(10));
                                a.add(0, "auto");
                                Vehiculo.registrar("vehiculos", a);
                                vehiculos.add(auto);
                                System.out.println("\n" + auto + "\n");
                                opcion1 = false;
                                break;
                            case 2:
                                ArrayList caon = new ArrayList();
                                for (String ele: caddatos4ruedas){
                                    System.out.println("Ingrese "+ele+"del vehículo: ");
                                    Vehiculo.comprobarRegistro("vehiculos", caddatos4ruedas, ele, caon);
                                }
                                Camion camion = new Camion((String)caon.get(0),(String)caon.get(1),(String)caon.get(2),(String)caon.get(3),(int)caon.get(4),
                                (double)caon.get(5),(String)caon.get(6),(String)caon.get(7),(String)caon.get(8),(String)caon.get(9),(double)caon.get(10));
                                caon.add(0, "camion");
                                Vehiculo.registrar("vehiculos", caon);
                                vehiculos.add(camion);
                                System.out.println("\n" + camion + "\n");
                                opcion1 = false;
                                break;
                            case 3:
                                ArrayList cata = new ArrayList();
                                for (String ele: caddatos4ruedas){
                                    System.out.println("Ingrese "+ele+"del vehículo: ");
                                    Vehiculo.comprobarRegistro("vehiculos", caddatos4ruedas, ele, cata);
                                }
                                Camioneta camioneta = new Camioneta((String)cata.get(0),(String)cata.get(1),(String)cata.get(2),(String)cata.get(3),(int)cata.get(4),
                                (double)cata.get(5),(String)cata.get(6),(String)cata.get(7),(String)cata.get(8),(String)cata.get(9),(double)cata.get(10));
                                cata.add(0, "camioneta");
                                Vehiculo.registrar("vehiculos", cata);
                                vehiculos.add(camioneta);
                                System.out.println("\n" + camioneta + "\n");
                                opcion1 = false;
                                break;
                            case 4:
                                ArrayList m = new ArrayList();
                                for (String ele: caddatosmoto){
                                    System.out.println("Ingrese "+ele+"del vehículo: ");
                                    Vehiculo.comprobarRegistro("vehiculos", caddatosmoto, ele, m);
                                    
                                }
                                Moto moto = new Moto((String)m.get(0),(String)m.get(1),(String)m.get(2),(String)m.get(3),(int)m.get(4),
                                (double)m.get(5),(String)m.get(6),(String)m.get(7),(String)m.get(8),(double)m.get(9));
                                m.add(8," ");
                                m.add(0,"moto");
                                Vehiculo.registrar("vehiculos", m);
                                vehiculos.add(moto);
                                System.out.println("\n" + moto + "\n");
                                opcion1 = false;
                                break;
                            case 5:
                                opcion1 = false;
                                break;
                        }
                        }
                        break;
                    case 3: // ACEPTAR OFERTA(POR TERMINAR)
                        Persona.comprobarUsuarioyClave("vendedores");                     
                        Vendedor.aceptarOferta();
                        break;
                    case 4: 
                        salir2 = true; 
                        break;
                    }
                }
                break;
            case 2:
                while(!salir3){  // OPCIONES DEL COMPRADOR
                    System.out.println("Seleccione una opción del menu:");
                    System.out.println("1. Ingresar un nuevo comprador.\n2. Ofertar por un vehiculo.\n3. Regresar.");
                    int select3 = Util.pedirIntEnIntervalo(1, 3);
                    switch(select3){ // INGRESAR NUEVO COMPRADOR (COMPLETADO)
                        case 1:
                            ArrayList<String> cadsC = new ArrayList<>();
                            for(String a:caddatos){
                                System.out.println("Ingrese " + a);
                                Persona.comprobarRegistro("compradores", caddatos, a, cadsC);
                            }
                            Comprador c = new Comprador(cadsC.get(0), cadsC.get(1), cadsC.get(2), cadsC.get(3), cadsC.get(4),cadsC.get(5));
                            Persona.registrar("compradores", cadsC);
                            compradores.add(c);
                            System.out.println("\n" + c + "\n");
                            break;
                        case 2: // OFERTAR POR UN VEHÍCULO (COMPLETADO) FALTA TESTEAR
                            double[] recorrido= {Vehiculo.MinRecorrido(vehiculos), Vehiculo.MaxRecorrido(vehiculos)};
                            int[] año = {Vehiculo.MinAño(vehiculos), Vehiculo.MaxAño(vehiculos)};
                            double[] precio = {Vehiculo.MinPrecio(vehiculos), Vehiculo.MaxPrecio(vehiculos)};
                            int num = 0;
                            boolean opcion3 = true;
                            while (opcion3){
                            System.out.println("Parametros de busqueda: \nSeleccione el parametro que quiera modificar o seleccione continuar");
                            System.out.println("1.Busqueda por tipo de vehículo\n2.Busqueda por recorrido\n3.Busqueda por año\n4.Busqueda por precio\n5.Continuar");
                            int opcion = Util.pedirIntEnIntervalo(1, 5);
                            switch (opcion){
                                case 1:
                                    System.out.println("Seleccione el tipo de vehículo que quiere buscar: ");
                                    System.out.println("1.Auto\n2.Camion\n3.Camioneta\n4.Moto");
                                    num = Vehiculo.seleccionarTipo();
                                    break;
                                case 2:
                                    recorrido = Comprador.rangoRecorrido(Vehiculo.MinRecorrido(vehiculos), Vehiculo.MaxRecorrido(vehiculos));
                                    break;
                                case 3:
                                    año = Comprador.rangoAño(Vehiculo.MinAño(vehiculos), Vehiculo.MaxAño(vehiculos));
                                    break;
                                case 4:
                                    precio = Comprador.rangoRecorrido(Vehiculo.MinPrecio(vehiculos), Vehiculo.MaxPrecio(vehiculos));
                                    break;
                                case 5:
                                    System.out.println("Vehículos disponibles para comprar: ");
                                        ArrayList<Vehiculo> vehiculosDisponibles = new ArrayList<>();
                                        for (Vehiculo element:vehiculos){ //Esto por huevos debe mejorarse, no puede quedar así, pero por ahora mi prioridad es que funcione
                                            if (num == 1)
                                                if (element instanceof Auto && Util.VerificarDoubleEnIntervalo(precio[0], precio[1], element.getPrecio())
                                                        && Util.VerificarDoubleEnIntervalo(recorrido[0], recorrido[1], element.getRecorrido())
                                                        && Util.VerificarIntEnIntervalo(año[0], año[1], element.getAño()))
                                                    vehiculosDisponibles.add(element);
                                            else if (num == 2)
                                                if (element instanceof Camion && Util.VerificarDoubleEnIntervalo(precio[0], precio[1], element.getPrecio())
                                                        && Util.VerificarDoubleEnIntervalo(recorrido[0], recorrido[1], element.getRecorrido())
                                                        && Util.VerificarIntEnIntervalo(año[0], año[1], element.getAño()))
                                                    vehiculosDisponibles.add(element);
                                            else if (num == 3)
                                                if (element instanceof Camioneta && Util.VerificarDoubleEnIntervalo(precio[0], precio[1], element.getPrecio())
                                                        && Util.VerificarDoubleEnIntervalo(recorrido[0], recorrido[1], element.getRecorrido())
                                                        && Util.VerificarIntEnIntervalo(año[0], año[1], element.getAño()))
                                                    vehiculosDisponibles.add(element);
                                            else if (num == 4)
                                                if (element instanceof Moto && Util.VerificarDoubleEnIntervalo(precio[0], precio[1], element.getPrecio())
                                                        && Util.VerificarDoubleEnIntervalo(recorrido[0], recorrido[1], element.getRecorrido())
                                                        && Util.VerificarIntEnIntervalo(año[0], año[1], element.getAño()))
                                                    vehiculosDisponibles.add(element);
                                            if (num == 0)
                                                if (Util.VerificarDoubleEnIntervalo(precio[0], precio[1], element.getPrecio())
                                                        && Util.VerificarDoubleEnIntervalo(recorrido[0], recorrido[1], element.getRecorrido())
                                                        && Util.VerificarIntEnIntervalo(año[0], año[1], element.getAño()))
                                                    vehiculosDisponibles.add(element);
                                        }
                                int n = 0;
                                OUTER:
                                while (true) {
                                    int tamaño = vehiculosDisponibles.size();
                                    System.out.println(vehiculosDisponibles.get(n));
                                    if (n==0) {
                                        System.out.println("1.Siguiente\n2.Ofertar");
                                        int seleccion = Util.pedirIntEnIntervalo(1, 2);
                                        if (seleccion == 1)
                                            n++;
                                        else
                                            break;
                                    } else if (n>0 && n<tamaño-1) {
                                        System.out.println("1.Siguiente\n2.atras\n3.Ofertar");
                                        int seleccion = Util.pedirIntEnIntervalo(1, 3);
                                        switch (seleccion) {
                                            case 1:
                                                n++;
                                                break;
                                            case 2:
                                                n--;
                                                break;
                                            default:
                                                break OUTER;
                                        }
                                    }
                                    else{
                                        System.out.println("1.atras\n2.Ofertar");
                                        int seleccion = Util.pedirIntEnIntervalo(1, 2);
                                        if (seleccion == 1)
                                            n--;
                                        else
                                            break;
                                    }   
                                }
                                String correo1 = Persona.comprobarUsuarioyClaveParaCorreo("compradores");
                                System.out.println("Ingrese su oferta en dolares para el vehiculo");
                                double ofer  = Util.pedirDoublePositivo();
                                //AQUÍ HAY QUE UTILIZAR LA FUNCIÓN DE LA CLASE OFERTAR, PERO HAY QUE MEJORARLA
                                Oferta.registrarOferta("Ofertas",vehiculosDisponibles.get(n).getPlaca(),correo1,ofer);
                                opcion3 = false;
                                break ;
                                }
                            }
                        case 3:
                            salir3 = true;
                            break;     
                    }
                }
                break;
            case 3:
                salir1 = true;
                break;
            }
        }
        
        //Reinicio de documentos  
    } 
}
